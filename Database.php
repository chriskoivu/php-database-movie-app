<?php
/* this class handles all the database operations */
		Class Database {
		   private $conn;
		   private $servername = "localhost";
		   private $username = "username";
		   private $password = "password";
		   private $dbname = "MovieApp";
		
		
		   protected function __construct() {
		      
		      try {
		         $this->create_database();
		         $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, 
		         $this->password);
		         // set the PDO error mode to exception
		         $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);          
		      }
		      catch(PDOException $e)
		      {
		        echo "Connection failed: " . $e->getMessage();
		      }
		
		      if (!$this->table_exists('Movies')) {
		      	$this->create_table();
		      }
		 
		   }
		
		
		   /* mysqli method to create database */
		   private function create_database(){
		       // Create connection
		       $conn = new mysqli($this->servername, $this->username, $this->password);
		       // Check connection
		       if ($conn->connect_error) {
		          die("Connection failed: " . $conn->connect_error);
		       }
		
		       // Create database
		       $sql = "CREATE DATABASE IF NOT EXISTS " . $this->dbname;
		       if ($conn->query($sql) === FALSE) {
		          echo "Error creating database: " . $conn->error;
		       }
		
		       $conn->close();
		   }
		
		
		   private function create_table() {
		       try {   
		    	  // sql to create table
		    	  $sql = "CREATE TABLE Movies (
		    	  film_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		    	  title VARCHAR(26) NOT NULL,
		    	  description VARCHAR(256),
		    	  release_date VARCHAR(30),
		    	  genre VARCHAR(30),
		    	  film_length VARCHAR(30),
		    	  director VARCHAR(256),
		    	  writer VARCHAR(256),
		    	  star VARCHAR(256),
		    	  costar VARCHAR(256),
		    	  format VARCHAR(10)
		        )";
		
		        // using exec because no results are returned
		        $this->conn->exec($sql);		         
		      }
		      catch(PDOException $e)
		      {
		        echo $sql . "<br>" . $e->getMessage();
		      }
		   }
		
		
		   private function table_exists($tablename) {
		   	 try {   
		       $sql = "select 1 from $tablename";
		       $result = $this->conn->query($sql);
		          return TRUE;
		       }catch(PDOException $e){		       
		          return FALSE;		        
		       }
		   }
		
		   protected function select_record_by_id($id) {
			
			try {
		    	 $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", 
                   $this->username, $this->password);
		    	  // set the PDO error mode to exception
		    	 $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
                         // prepare sql
		    	  $sql = $conn->prepare("SELECT * FROM Movies WHERE film_id = :film_id");

                          // bind parameter                          
			  $sql->bindParam(':film_id', $this->filter_input($id));			  
		
		    	  // execute the query
		    	  $sql->execute();
				  $result = $sql->fetch(PDO::FETCH_ASSOC);
                  return $result;		    	  
		   	}
			catch(PDOException $e)
		    	{
		    	  echo $sql . "<br>" . $e->getMessage();
		    	}
			    $conn = null;
		
		   }
		
		    protected function select_record_by_genre($genre) {
			
			try {
		    	 $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", 
                          $this->username, $this->password);
		    	  // set the PDO error mode to exception
		    	 $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
                 // prepare sql
		    	  $sql = $conn->prepare("SELECT * FROM Movies WHERE genre = :genre");
                  // bind parameter                          
			      $sql->bindParam(':genre', $this->filter_input($genre));	
		    	  // execute the query
		    	  $sql->execute();
                   // set the resulting array to associative
		          $result = $sql->setFetchMode(PDO::FETCH_ASSOC); 
		          $arr = $sql->fetchAll();
		          return $arr;		   	  
		   	}
			catch(PDOException $e)
		    	{
		    	  echo $sql . "<br>" . $e->getMessage();
		    	}
			    $conn = null;		
		   }
		   
		   protected function select_all() {
			   try {
		    	   $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", 
                   $this->username, $this->password);
		    	  // set the PDO error mode to exception
		    	  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		    	  $sql = "SELECT * FROM Movies";
		          $stmt = $conn->prepare($sql);
		          $stmt->execute();
		          // set the resulting array to associative
		          $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
		          $arr = $stmt->fetchAll();
		          return $arr;
				}catch(PDOException $e){
		    	  echo $sql . "<br>" . $e->getMessage();
		    	}
			    $conn = null;
		   }
		
		   protected function insert_record($arr) {
		      try {
		        $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", 
                        $this->username, $this->password);
		    	  // set the PDO error mode to exception
		   	  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);			

		    	
			    // prepare sql and bind parameters
			    $stmt = $conn->prepare("INSERT INTO Movies (title, description,release_date,
			    genre, film_length, director, writer, star, costar, format)
			    VALUES (:title, :description, :release_date, :genre, :film_length, 
			    :director, :writer, :star, :costar, :format)");			
			    $stmt->bindParam(':title', $title);
			    $stmt->bindParam(':description', $description);
			    $stmt->bindParam(':release_date', $release_date);
			    $stmt->bindParam(':genre', $genre);
			    $stmt->bindParam(':film_length', $film_length);
			    $stmt->bindParam(':director', $director);
			    $stmt->bindParam(':writer', $writer);
			    $stmt->bindParam(':star', $star);
			    $stmt->bindParam(':costar', $costar);
			    $stmt->bindParam(':format', $format);  
			
			    // insert a row
			    $title = $this->filter_input($arr['title']);
			    $description = $this->filter_input($arr['description']);
			    $release_date = $this->filter_input($arr['release_date']);
			    $genre = $this->filter_input($arr['genre']);
			    $film_length = $this->filter_input($arr['film_length']);
			    $director = $this->filter_input($arr['director']);
			    $writer = $this->filter_input($arr['writer']);
			    $star = $this->filter_input($arr['star']);
			    $costar = $this->filter_input($arr['costar']);
			    $format = $this->filter_input($arr['format']);
			   
			    $stmt->execute();
			
			   
			
			    echo "New record created successfully";
			    }
			catch(PDOException $e)
			    {
			    echo "Error: " . $e->getMessage();
			    }
			    $conn = null;
			}
		
		
		   protected function delete_record($id) {
		      
		
			try {
		    	  $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", 
                   $this->username, $this->password);
		    	  // set the PDO error mode to exception
		    	  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		      	  // sql to delete a record
		      	  $sql = "DELETE FROM Movies WHERE film_id=" . $id;
		
		    	  // use exec() because no results are returned
		    	  $conn->exec($sql);
		          echo "Record deleted successfully";
		    	 }
			 catch(PDOException $e)
		    	 {
		    	    echo $sql . "<br>" . $e->getMessage();
		    	 }
			 $conn = null;
		 }
		
		
        protected function update_record($arr, $film_id) {
           // Create connection
           $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

           // Check connection
           if ($conn->connect_error) {
             die("Connection failed: " . $conn->connect_error);
           }
             $sql = "UPDATE Movies SET title=?, description=?, release_date=?, genre=?, film_length=?, 
             director=?, writer=?, star=?,costar=?,format=? WHERE film_id=?";
             // prepare and bind
             $stmt = $conn->prepare($sql);           
           
           
             /* strip out html tags */
             $title = $this->filter_input($arr['title']);
			    $description = $this->filter_input($arr['description']);
			    $release_date = $this->filter_input($arr['release_date']);
			    $genre = $this->filter_input($arr['genre']);
			    $film_length = $this->filter_input($arr['film_length']);
			    $director = $this->filter_input($arr['director']);
			    $writer = $this->filter_input($arr['writer']);
			    $star = $this->filter_input($arr['star']);
			    $costar = $this->filter_input($arr['costar']);
			    $format = $this->filter_input($arr['format']);
			    
			    /*bind parameters and their values by reference */
           	 $stmt->bind_param('ssssssssssd', $title, $description, $release_date, 
             $genre, $film_length, $director, $writer,$star, 
             $costar,$format,$film_id);
             
             /*execute the query */
             $stmt->execute();
             $stmt->close();
             $conn->close();                
        }		
		
		  public function close_db() {
		       $this->conn = null;   
		
		  }

          /* remove all html tags and values behind them */
          private function filter_input($str) {
             return filter_var($str, FILTER_SANITIZE_STRING);
          }
    }  
?>