<HTML>
<HEAD>
  <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>    
  <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <TITLE>Christopher M Koivu - Assignment 6</TITLE>
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/assign6.css">

<?php  
   require_once('Request.php');
   $req = New Request();  
?>
 
  <script>
      $(function() {            
        $( "#txtReleaseDate" ).datepicker(); 
      });  
  </script>

 
</HEAD>
<BODY><h1> Christopher M Koivu </h1>
	 <h1> Movie Database Application </h1>

     <div class="wrapper">  
	     <p style ="width:70%;background-color:#f2f2f2;margin-left:auto;margin-right:auto;">
		    This is the the Add Movie Info page. To Add a Movie Title,
			enter the movie information in the form fields and click Save. If 
			you wish not to add movie information, click Cancel
		 </p>
	  </div>

     
     <div class="wrapper" style="width:80%;">   
         <form method="post">
              <table> 
		          <tr><td>Title:</td><td> <input type="text" name="title" id="txtTitle"></td></tr>
		          <tr><td>Description:</td><td> <textarea rows="4" cols = "35" name="description" id="txtDesc"></textarea></td></tr>
		          <tr><td>Release Date:</td><td> <input type="text" name="release_date" id="txtReleaseDate"></td></tr>
		          <tr><td>Genre:</td><td> 
		           <select name="genre" id="txtGenre">
						  <option value="Action">Action</option>
						  <option value="Adventure">Adventure</option>
						  <option value="Comedy">Comedy</option>
						  <option value="Drama">Drama</option>
						  <option value="Horror">Horror</option>
						  <option value="Western">Western</option>
						</select> 
		          </td></tr>
		          <tr><td>Film Length:</td>
		          <td><input type="text" name="film_length_hours" id="txtFilmHrs" style="width:60px;">Hours
		          <input type="text" name="film_length_minutes" id="txtFilmMin" style="width:60px; margin-left:10px;">Minutes</td>
		          </tr>
		          <tr><td>Director:</td><td><input type="text" name="director" id="txtDirector"></td></tr>
		          <tr><td>Writer:</td><td> <input type="text" name="writer" id="txtWriter"></td></tr>
		          <tr><td>Star:</td><td> <input type="text" name="star" id="txtStar"></td></tr>
		          <tr><td>Costar:</td><td> <input type="text" name="costar" id="txtCostar"></td></tr>
		          <tr><td>Format:</td><td> 
		          	<input type="radio" name="format" value="DVD" checked>DVD<br>
  						<input type="radio" name="format" value="Bluray"> Bluray<br>
 						<input type="radio" name="format" value="DVD/Bluray"> DVD & Bluray		          
		          </td></tr>	
		          
		          <tr><td style="background-color:transparent;"><input type="submit" name = "submit" value="Save" ></td>
				  <td style="background-color:transparent;"><input type="submit" name = "cancel" value="Cancel" ></tr>	          
             </table>	
         </form>
     </div>

     <?php
         if ( isset( $_POST['submit']) ) {            
           $req->input_db_record($_POST);      
         }
		 if(isset( $_POST['cancel']))  { 
		   require_once('Request.php');
           $req = New Request();
		   $req->cancel_request();
         }
     ?>


	
</BODY>
</HTML>