<!DOCTYPE HTML>
<html>
  <head>
     <title>Chris Koivu - Assignment 6</title>	 
     <link rel="stylesheet" href="../css/style.css">
     <?php	    
	    require_once('Request.php');
		$req = New Request();        
     ?>
  </head>
 
  <body>
  
     <div class="wrapper">  
	     <p style ="width:70%;background-color:#ff1a1a;margin-left:auto;margin-right:auto;">
		    This is the the Movie Delete page. To delete the movie from the
			database, click Delete Movie. If you wish not to delete the movie, 
			click Cancel
		 </p>
	  </div>
	  
      <?php
	     /* 		 
		   the post variable 'deletetitle' should be set as this is the 
		   last post saved in the post array 
		 */
    	 if(isset($_POST['deletetitle'])){
			/* all the records in db were retrieved and stored to session on index page */
			/* retrieving the records here to get the movie name */
			$records = $req->get_session_data('records');
			
			/* get the index of the current record */
			$index = $_POST['delete'];  
			
			/* saving the movie name and id to session */
			$req->set_session_data('movie_name', $records[$index]['title']);
            $req->set_session_data('movie_id', $records[$index]['film_id']);            
		 }
	   ?>
	  <h1> Movie Name: <?php echo $req->get_session_data('movie_name');  ?></h1>
      <div class="wrapper">            
     	<form method="post">
			<table>
				<tr><td>
				Are you sure you want to delete this movie from the database?
				</td></tr>
				 
				<tr><td>
				  <input type="submit" name = "<?php echo $req->get_session_data('movie_id'); ?>" value="Delete Movie">
				<input type="submit" name = "cancel" value="Cancel" ></td></tr>
			</table>
		</form>
		<?php
		    
		   if ( isset( $_POST[$req->get_session_data('movie_id')]) ) { 		
              $req->delete_db_record($req->get_session_data('movie_id'));			  
           }	
		   
		   if(isset( $_POST['cancel']))  { 		      
              $req = New Request();
		      $req->cancel_request();
           }
        ?>		  
	  </div>
  </body>  
</html>



